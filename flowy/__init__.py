from ctx import Context
from st3m.input import InputState
from st3m.application import Application, ApplicationContext

import st3m.run

# squint is doubled for easy animation code
IMAGE_NAMES = [
    "fadein/Image1",
    "fadein/Image2",
    "fadein/Image3",
    "fadein/Image4",
    "fadein/Image5",
    "talk",
    "talk-t",
    "smile",
    "smile-t",
    "anger",
    "anger-t",
    "upset",
    "upset-t",
    "mad",
    "mad-t",
    "squint",
    "squint",
]


class FlowyImg:
    def __init__(self, img_name: str) -> None:
        self.img_name = img_name

    def draw(self, ctx: Context) -> None:
        ctx.image(
            self.img_name,
            -125,
            -125,
            250,
            250,
        )


class Flowy(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self.images: list = []
        for n in IMAGE_NAMES:
            self.images.append(FlowyImg(f"{self.app_ctx.bundle_path}/assets/{n}.jpg"))
        self.showing: int = 0
        self.fadein: bool = True
        self.talk: bool = False
        self.delta_ms: int = 0
        self.last_acc: tuple = (0, 0, 0)

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        self.delta_ms += delta_ms
        # update delta_acc
        # print(self.last_acc)
        acc = ins.imu.acc
        delta_acc = (
            acc[0] - self.last_acc[0],
            acc[1] - self.last_acc[1],
            acc[2] - self.last_acc[2],
        )
        # print(delta_acc, acc)
        self.last_acc = acc
        # remove gravity and round to int
        delta_acc = (
            abs(round(delta_acc[0])),
            abs(round(delta_acc[1])),
            abs(round(delta_acc[2])),
        )
        # tmp = 1 if delta_ms > 1000 else 0
        tmp: int = self.delta_ms // 1000
        if tmp == 0:
            return
        self.delta_ms = self.delta_ms % 1000
        # run once at start of app
        if self.fadein:
            self.showing += tmp
            if self.showing == 5:
                self.fadein = False
            return

        # if x or y are moving:
        if delta_acc[0] > 0 or delta_acc[1] > 0 and delta_acc[2] == 0:
            self.showing += 2
        # if x and y are moving:
        elif delta_acc[0] > 0 and delta_acc[1] > 0 and delta_acc[2] == 0:
            self.showing = 15
        # if z is moving:
        elif delta_acc[2] > 0:
            self.showing = 13
        elif delta_acc == (0, 0, 0) and self.showing > 6:
            self.showing -= 2

        # jump between normal and -t images
        self.showing += -1 if self.talk else 1
        self.talk = self.showing % 2 == 0

        # print debug if clamp is needed
        if self.showing > 16:
            print("max" + str(self.showing))
            self.showing = 16
        if self.showing < 0:
            print("min" + str(self.showing))
            self.showing = 0

    def draw(self, ctx: Context):
        ctx.image_smoothing = False
        self.images[self.showing].draw(ctx)


if __name__ == "__main__":
    st3m.run.run_view(Flowy(ApplicationContext()))
